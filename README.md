# Crawler



## Description

Ce projet fait office de rendu pour le premier TP d'Indexation Web. 
À partir d'une URL d'entrée unique (https://ensai.fr/), un crawler télécharge une page, puis attend au moins cinq secondes avant de télécharger la page suivante. Le programme trouve d'autres pages à explorer en analysant les balises de liens trouvées dans les documents précédemment explorés. Ce dernier se termine lorsque le crawler arrive à 50 urls trouvées ou si il ne trouve plus de liens à explorer. Une fois terminé, le programme écrit dans un fichier crawled_webpages.txt toutes les urls trouvées. Finalement, il y a deux règles à respecter pour ce projet. A savoir, le respecter la politeness et ne pas crawler un siteweb qui nous l'interdit.

## Installation

Afin de pouvoir utiliser ce programme, veuillez installer les dépendances nécessaires.

```
git clone https://gitlab.com/BadrKe/crawler.git
cd crawler
pip install -r requirements.txt
```

## Lancement du programme

Pour lancer le programme, veuillez tapez les lignes de commandes suivantes dans votre terminal.

```
cd crawler
python3 main.py
```

## Auteur

Badr Kebrit