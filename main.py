from crawler import Crawler

if __name__ == '__main__':
    crawler = Crawler(first_url="https://ensai.fr/", nb_to_find=50)
    crawler.search_websites()
    crawler.to_txt()