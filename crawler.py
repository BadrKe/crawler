import urllib.robotparser
import requests
from bs4 import BeautifulSoup
import socket
import time


class Crawler():

    def __init__(self, first_url : str, nb_to_find : int):
        self.first_url = first_url
        self.list_url = [first_url]
        self.nb_to_find = nb_to_find

    def can_be_visited(self, url: str):
        """
        Cette méthode prend en argmument l'url d'un site web et vérifie si le site peut être crawlé.
        ELle renvoie True si c'est le cas et False sinon.

        Arguments :
            url (str) : Chaîne de caractère représentant l'url à tester.
        """
        socket.setdefaulttimeout(3)
        try :
            uri = urllib.request.urlparse(url)
            url_robot = uri.scheme + '://' + uri.netloc + '/robots.txt'
            robot = urllib.robotparser.RobotFileParser()
            robot.set_url(url_robot)
            robot.read()
            return robot.can_fetch("*", url)
        except:
            return False

    def find_links(self, url):
        """
        Cette méthode prend en argmument l'url d'un site web et détecte tout les liens vers d'autre sites web présent sur la page.
        De plus, si on a le droit de crawler ces liens alors ils seront ajouter dans la liste des URL scrappés. (A condition qu'ils ne soient pas déjà dans cette liste)

        Arguments :
            url (str) : Chaîne de caractère représentant l'url du site web à scrapper.
        """
        try:
            request = requests.get(url, timeout=5)
            soup = BeautifulSoup(request.content, 'html.parser')


            finded_link = []
            for a in soup.find_all('a', href=True):
                new_link = a['href']
                finded_link.append(new_link)
            
            while (len(self.list_url) < self.nb_to_find) and (len(finded_link) > 0):
                new_link = finded_link.pop(0)
                if self.can_be_visited(new_link) and (new_link not in self.list_url):
                    self.list_url.append(new_link)
        except:
            pass

    def search_websites(self):
        """
        Cette méthode alimente la liste des urls trouvés. 
        Tant que la longueur de la liste n'est pas celle souhaité et qu'il est toujours possible d'aller chercher d'autre liens, alors la boucle continue.
        """
        compteur = 0
        while (len(self.list_url) < self.nb_to_find) and (compteur <= len(self.list_url)):
            url = self.list_url[compteur]
            self.find_links(url)
            compteur += 1
            time.sleep(5)
    def to_txt(self):
        """
        Cette méthode permet d'écrire les liens des urls scrappés dans le fichier text 'crawled_webpages.txt'.
        """
        with open("crawled_webpages.txt", "w") as file:
            for url in self.list_url:
                file.write(str(url) + " \n")
